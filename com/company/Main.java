package com.company;

import com.company.additional.Finance;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //init scanner
        Scanner scan = new Scanner(System.in);
        //init finance
        Finance market = new Finance(5, 5, 5);
        //enter amount of money
        System.out.println("Enter the amount of money: ");
        int amount = Integer.parseInt(scan.nextLine());
        //looking for services
        market.printCapable(amount);
        //enter name of the bank
        System.out.println("Enter the bank, exchanger or black market:");
        String name = scan.nextLine();
        float change = market.exchange(amount, name);
        if (change >= 0) {
            System.out.println(String.format("you money now: $%.2f", change));
        } else {
            System.out.println("Sorry, something went wrong :(");
        }
    }
}