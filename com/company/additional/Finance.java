package com.company.additional;

public class Finance {
    private Org[] organizations;

    public Finance(int banks, int exchangers, int blackMarkets) {
        this.organizations = Generator.generateOrganizations(banks, exchangers, blackMarkets);
    }

    public void printCapable(double amount) {
        System.out.println("Capable organizations: ");
        for (Org organization : organizations) {
            if (organization.getLimit() >= amount) {
                System.out.println(organization.getName());
            }
        }

    }

    public float exchange(float money, String name) {
        float result = 0;
        for (Org organization : organizations) {
            if (organization.getName().equalsIgnoreCase(name)) {
                result = organization.change(money);
                break;
            }
        }

        return result;
    }
}
