package com.company.additional;

public class Org {
    private String name;
    private float course;
    private int limit;

    public Org(String name, float course, int limit) {
        this.name = name;
        this.course = course;
        this.limit = limit;
    }

    public float change(float value) {
        if (value < limit) {
            return value / course;
        }
        return -1;
    }

    public String getName() {
        return name;
    }

    public int getLimit() {
        return limit;
    }
}
