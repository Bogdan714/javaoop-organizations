package com.company.additional;

import com.company.models.*;

public class Generator {
    public static Bank[] generateBanks(int number) {
        Bank[] banks = new Bank[number];
        for (int i = 0; i < banks.length; i++) {
            banks[i] = new Bank("Bank " + i, 25 + ((float) Math.random() * 4) - 2);
        }
        return banks;
    }

    public static Exchanger[] generateExchangers(int number) {
        Exchanger[] exchangers = new Exchanger[number];
        for (int i = 0; i < exchangers.length; i++) {
            exchangers[i] = new Exchanger("Exchanger " + i, 26 + ((float) Math.random() * 4) - 2, 1000 + (int) (Math.random() * 1000));
        }
        return exchangers;
    }

    public static BlackMarket[] generateBlackMarkets(int number) {
        BlackMarket[] blackMarkets = new BlackMarket[number];
        for (int i = 0; i < blackMarkets.length; i++) {
            blackMarkets[i] = new BlackMarket("BM " + i, 29 + ((float) Math.random() * 4) - 2);
        }
        return blackMarkets;
    }

    public static Org[] generateOrganizations(int banksVal, int exchangersVal, int BMsVal) {
        Org[] organizations = new Org[banksVal + exchangersVal + BMsVal];
        Bank[] banks = Generator.generateBanks(banksVal);
        Exchanger[] exchangers = Generator.generateExchangers(exchangersVal);
        BlackMarket[] blackMarkets = Generator.generateBlackMarkets(BMsVal);
        for (int i = 0; i < banksVal; i++) {
            organizations[i] = banks[i];
        }
        for (int i = banksVal; i < exchangersVal + banksVal; i++) {
            organizations[i] = exchangers[i - banksVal];
        }
        for (int i = exchangersVal + banksVal; i < BMsVal + banksVal + exchangersVal; i++) {
            organizations[i] = blackMarkets[i - BMsVal - banksVal];
        }
        return organizations;
    }

}
