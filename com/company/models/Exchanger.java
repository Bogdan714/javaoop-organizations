package com.company.models;

import com.company.additional.Org;

public class Exchanger extends Org {
    private float commission;

    public Exchanger(String name, float course, int limit) {
        super(name, course, limit);
        this.commission = 0.1f;
    }

    @Override
    public float change(float value) {
        return super.change(value) * (1 - commission);
    }
}
