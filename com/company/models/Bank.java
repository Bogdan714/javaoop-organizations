package com.company.models;

import com.company.additional.Org;

public class Bank extends Org {
    private float commission;

    public Bank(String name, float course) {
        super(name, course, 150_000);
        this.commission = 0.05f;
    }

    @Override
    public float change(float value) {
        return super.change(value) * (1 - commission);
    }
}
