package com.company.models;

import com.company.additional.Org;

public class BlackMarket extends Org {

    public BlackMarket(String name, float course) {
        super(name, course, Integer.MAX_VALUE);
    }

    @Override
    public float change(float value) {
        return super.change(value);
    }
}
